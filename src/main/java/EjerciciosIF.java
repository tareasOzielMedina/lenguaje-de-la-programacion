/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.util.Scanner;
/**
 *
 * @author Oziel
 */
public class EjerciciosIF {
    
    static void ImprimirMensaje(String sMensaje){
        System.out.println(sMensaje);
    } 
    static void separador(){
        ImprimirMensaje("-----------------------------------");
    }
    static void Encabezado(){
        ImprimirMensaje("Universidad Autonoma de Campeche");
        ImprimirMensaje("Facultad de Ingenieri­a");
        ImprimirMensaje("Ing. Sistemas Computacionales");
        ImprimirMensaje("Oziel Antonio Medina Canul");
        separador();
    }
    static void Piepagina(){
        separador();
        ImprimirMensaje("UACAM      ISC        OAMC");
    }
    static void menu(){
        ImprimirMensaje("");
        separador();
        ImprimirMensaje("Indice de Ejercicios");
        ImprimirMensaje("");
        ImprimirMensaje("1.- Comparacion de dos numeros");
        ImprimirMensaje("10.- Cercania entre tres numeros");
        separador();
        Scanner entrada = new Scanner(System.in);
        ImprimirMensaje("Escribe la opción deseada: ");        
        int iOpcion = entrada.nextInt();
        submenu(iOpcion);
    }
    static void submenu(int opcion){
       if (opcion == 1){
           ejercicio1();   
        }
       
       if (opcion == 10){
           ejercicio10();
        }
    }
    static void ejercicio1(){
        separador();
        Scanner A = new Scanner(System.in);
        ImprimirMensaje("Escribe el primer numero: ");
        int primero = A.nextInt();
        ImprimirMensaje("Escribe el segundo numero: ");
        int segundo = A.nextInt();
        ImprimirMensaje("");
            if(primero > segundo){
            ImprimirMensaje(primero+" "+"es el mayor");
            ImprimirMensaje(segundo+" "+"es el menor");
            }else{
                if(primero < segundo){
                ImprimirMensaje(segundo+" "+"es el mayor");
                ImprimirMensaje(primero+" "+"es el menor");
                }else{
                    if(primero == segundo);
                    ImprimirMensaje("Los dos numeros son iguales");
                }
            }
        repetidor();
    }
    static void ejercicio10(){
        separador();
        Scanner B = new Scanner(System.in);
        ImprimirMensaje("Escribe el primer numero: ");
        int priNum = B.nextInt();
        ImprimirMensaje("Escribe el segundo numero: ");
        int segNum = B.nextInt();
        ImprimirMensaje("Escribe el tercer numero: ");
        int terNum = B.nextInt();
            int difPT = terNum - priNum;
            if (difPT < 0){
             difPT = difPT * -1;
            }
            int difST = terNum - segNum;
            if(difST < 0){
             difST = difST * -1;    
            }
            if (difPT == difST){
             ImprimirMensaje("El tercer numero se encuentra a la misma distancia del primer y segundo numero");   
            }else{
                if(difPT < difST){
                 ImprimirMensaje("El tercer numero esta mas cerca del primer numero");
                }else{
                   ImprimirMensaje("El tercer numero esta mas cerca del segundo numero");
                }
            }
        repetidor();
    }
    static void repetidor(){
        Scanner entrada = new Scanner (System.in);
        separador();
        ImprimirMensaje("¿Desea realizar alguna otra operación?");
        ImprimirMensaje("       1 = Sí         0 = No");
        int evaluar = entrada.nextInt();
        evaluador(evaluar);
    }
    static void evaluador(int evaluar){
        if (evaluar == 1){
           menu();   
        }
       
        if (evaluar == 0){
        }
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Ejercicios 1 y 10
        Encabezado();
        menu();
        Piepagina();
    }
    
}
